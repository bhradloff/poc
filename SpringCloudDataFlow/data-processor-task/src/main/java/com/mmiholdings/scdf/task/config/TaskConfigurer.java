package com.mmiholdings.scdf.task.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.task.configuration.DefaultTaskConfigurer;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;

@Component
@Profile("kubernetes")
public class TaskConfigurer extends DefaultTaskConfigurer {


    public TaskConfigurer(@Qualifier("taskDataSource")DataSource dataSource){
        super(dataSource);
    }
}
