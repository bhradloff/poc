package com.mmiholdings.scdf.task.config;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Slf4j
@ToString
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Configuration
@PropertySource(value = "classpath:batch_datasource.properties", ignoreResourceNotFound = true)
@ConfigurationProperties("batch")
public class TaskConfig {
/**
 * batch:
 *   datasource:
 *     password: password
 *     username: postgres
 *     driver-class-name: org.postgresql.Driver
 *     url: jdbc:postgresql://10.104.154.32:5432/postgres?currentSchema=${spring.jpa.properties.hibernate.default_schema}
 *   jpa:
 *     hibernate:
 *       ddl-auto: update
 *     show-sql: true
 *     properties:
 *       hibernate:
 *         default_schema: batch_test
 *     database-platform: org.hibernate.dialect.PostgreSQL94Dialect
 */
    private DataSourceConfig datasource;
    private JpaConfig jpa;

    @ToString
    @Getter
    @Setter
    @AllArgsConstructor
    @NoArgsConstructor
    static public class DataSourceConfig {
        private String password;
        private String username;
        private String driverClassName;
        private String url;
    }
    @ToString
    @Getter
    @Setter
    @AllArgsConstructor
    @NoArgsConstructor
    static public class JpaConfig {
        private String databasePlatform;
        private Boolean showSql;
        private HibernateConfig hibernate;
        private JpaPropertiesConfig properties;
    }
    @ToString
    @Getter
    @Setter
    @AllArgsConstructor
    @NoArgsConstructor
    static public class JpaPropertiesConfig {
        private HibernateConfig hibernate;
    }
    @ToString
    @Getter
    @Setter
    @AllArgsConstructor
    @NoArgsConstructor
    static public class HibernateConfig {
        private String ddlAuto;
        private String defaultSchema;
    }
}
