package com.mmiholdings.scdf.task;

import com.mmiholdings.scdf.task.repository.SubscriberRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class DataProcessorTask implements ApplicationRunner {
    private SubscriberRepository subscriberRepository;
    public  DataProcessorTask(final SubscriberRepository subscriberRepository) {
        this.subscriberRepository =subscriberRepository;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        subscriberRepository.findAll().forEach(data -> {
            try {
                Thread.sleep(100000);
            } catch (InterruptedException e) {
                log.error("something happened :( {}", e.getMessage());
            }
            log.info("data: {}", data.toString());
        });
    }
}
