package com.mmiholdings.scdf.task;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.task.configuration.EnableTask;

@SpringBootApplication
@EnableTask
public class DataProcessorTaskApplication {

    public static void main(String[] args) {
        SpringApplication.run(DataProcessorTaskApplication.class, args);
    }

}
