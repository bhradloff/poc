package com.mmiholdings.scdf.task.repository;

import com.mmiholdings.scdf.task.model.SubscriberEntity;
import org.springframework.data.repository.CrudRepository;

public interface SubscriberRepository extends CrudRepository<SubscriberEntity, Long> {
}
