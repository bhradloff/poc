package com.mmiholdings.scdf.task.config.datasource;

import com.mmiholdings.scdf.task.config.TaskConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

@Configuration
public class TaskDataSource {

    @Autowired
    private TaskConfig taskConfig;

    @Bean
    @Profile("kubernetes")
    @Qualifier("taskDataSource")
    public DataSource dataSource(){
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(taskConfig.getDatasource().getDriverClassName());
        dataSource.setUrl(taskConfig.getDatasource().getUrl());
        dataSource.setUsername(taskConfig.getDatasource().getUsername());
        dataSource.setPassword(taskConfig.getDatasource().getPassword());
        dataSource.setSchema(taskConfig.getJpa().getProperties().getHibernate().getDefaultSchema());
        return dataSource;
    }
}
