package com.ayo.batch.dataprocessor.config.datasource;

import com.ayo.batch.dataprocessor.config.BatchConfig;
import org.springframework.batch.core.configuration.annotation.BatchConfigurer;
import org.springframework.batch.core.configuration.annotation.DefaultBatchConfigurer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

@Configuration
public class BatchDataSource  {

    @Autowired
    private BatchConfig batchConfig;

    @Bean
    public BatchConfigurer configurer(@Qualifier("batchDataSource")DataSource dataSource){
        return new DefaultBatchConfigurer(dataSource);
    }

    @Bean
    @Qualifier("batchDataSource")
    public DataSource dataSource(){
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(batchConfig.getDatasource().getDriverClassName());
        dataSource.setUrl(batchConfig.getDatasource().getUrl());
        dataSource.setUsername(batchConfig.getDatasource().getUsername());
        dataSource.setPassword(batchConfig.getDatasource().getPassword());
        dataSource.setSchema(batchConfig.getJpa().getProperties().getHibernate().getDefaultSchema());
        return dataSource;
    }
}
