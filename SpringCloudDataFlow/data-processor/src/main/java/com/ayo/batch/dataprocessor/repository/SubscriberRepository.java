package com.ayo.batch.dataprocessor.repository;

import com.ayo.batch.dataprocessor.model.SubscriberEntity;
import org.springframework.data.repository.CrudRepository;

public interface SubscriberRepository extends CrudRepository<SubscriberEntity, Long> {
}
