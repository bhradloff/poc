package com.ayo.batch.dataprocessor.processor;

import com.ayo.batch.dataprocessor.config.BatchConfig;
import com.ayo.batch.dataprocessor.model.SubscriberEntity;
import com.ayo.batch.dataprocessor.repository.SubscriberRepository;
import com.mmiholdings.scdf.data.Subscriber;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Processor;
import org.springframework.integration.annotation.Transformer;

import java.util.UUID;

@Slf4j
@EnableBinding(Processor.class)
public class DataProcessor {

    @Value("${spring.datasource.url:NA}")
    private String dbName;
    private UUID uuid;

    private BatchConfig batchConfig;
    private SubscriberRepository repo;

    @Autowired
    public DataProcessor(final BatchConfig batchConfig, final SubscriberRepository repo) {
        this.batchConfig = batchConfig;
        this.repo = repo;
        log.info(batchConfig.toString());
        uuid = UUID.randomUUID();
    }

//    @StreamListener(Processor.INPUT)
//    @SendTo(Processor.OUTPUT)
    @Transformer(inputChannel = Processor.INPUT, outputChannel = Processor.OUTPUT)
    public String dataProcessor(Subscriber subscriber) {
        log.info("Running Version {}", getClass().getPackage().getImplementationVersion());
        log.info("Found the following value for - DBName [{}] Subscriber [{}]", dbName, subscriber.toString());
        SubscriberEntity savedEntity = null;
        try {
            savedEntity = repo.save(SubscriberEntity.builder()
                    .subscriberNumber(Integer.toString(subscriber.getSubscriberId()))
                    .source(uuid.toString())
                    .build());
        } catch (Exception e) {
            log.error("DB Error occurred : {}", e.getMessage());
        }
        return "subscriber: " + savedEntity;
    }
}
