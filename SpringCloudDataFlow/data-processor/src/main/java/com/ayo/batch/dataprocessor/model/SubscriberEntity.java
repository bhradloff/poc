package com.ayo.batch.dataprocessor.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Builder
@Entity
@Table(name = "subscriber")
public class SubscriberEntity {
    @SequenceGenerator(name = "subscriberSequence", sequenceName = "sub_seq", allocationSize = 1)
    @GeneratedValue(generator = "subscriberSequence", strategy = GenerationType.SEQUENCE)
    @Id
    private long id;

    @Column(name  = "subscriber_number")
    private String subscriberNumber;
    @Column(name  = "source")
    private String source;
}
