package com.mmiholdings.scdf.source.subscribersource.source;

import com.mmiholdings.scdf.data.Subscriber;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.integration.annotation.InboundChannelAdapter;
import org.springframework.integration.annotation.Poller;
import org.springframework.stereotype.Component;

import java.util.Random;

@Slf4j
@Component
public class SubscriberSource {

    @InboundChannelAdapter(
            value = Source.OUTPUT,
            poller = @Poller(fixedDelay = "100", maxMessagesPerPoll = "1")
//            poller = @Poller(fixedDelay = "10000", maxMessagesPerPoll = "1")
    )
    public Subscriber generateData() {
        Subscriber sub = Subscriber.builder().subscriberId(new Random().nextInt()).build();
        log.info("Subscriber = {}", sub);
        return sub;
    }
}
