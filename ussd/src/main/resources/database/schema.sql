# create schema ussdv2;
# grant all privileges on ussdv2.* to ayo@'%';

use ussdv2;

drop table if exists audit_log;
drop table if exists journey;
drop table if exists user_journey;
drop table if exists message_resource;
drop table if exists journey_step;
drop table if exists application_service;

create table application_service
(
    id                integer      not null primary key,
    name              varchar(128) not null,
    version           varchar(12),
    type              varchar(32),
    generates_journey smallint
);

create table journey_step
(
    id                     integer not null auto_increment,
    message_resource_id    char(32) default (""),
    activation_code        char(32),
    application_service_id integer  default (-1),
    version                integer,
    service_code           char(32),
    primary key (id)
    #     constraint fk_application_id foreign key (application_service_id)
    #         references application_service (id)
);

create table message_resource
(
    message_key char(64) not null,
    locale      char(6)  not null,
    value       text,
    primary key (message_key, locale)
);

create table user_journey
(
    id              integer not null primary key,
    journey_step_id integer not null,
    response        text,
    sequence        integer,
    direct_access   integer,
    CONSTRAINT fk_journey_step FOREIGN KEY (journey_step_id)
        REFERENCES journey_step (id)
);

create table journey
(
    current_step_id integer not null,
    next_step_id    integer not null,
    valid_response  text,
    active          smallint,
    CONSTRAINT fk_current_step FOREIGN KEY (current_step_id)
        references journey_step (id),
    CONSTRAINT fk_next_step FOREIGN KEY (next_step_id)
        REFERENCES journey_step (id)
);

create table audit_log
(
    id       integer auto_increment not null primary key,
    request  text,
    response text,
    created  datetime,
    updated  datetime
);





