package com.ayoholdings.ussd.system.version;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.actuate.info.Info;
import org.springframework.boot.actuate.info.InfoContributor;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class VersionInfoContributor implements InfoContributor {
    private VersionService version;
    public VersionInfoContributor(VersionService version){
        this.version = version;
    }

    @Override
    public void contribute(Info.Builder builder) {

        builder.withDetail("Version", version.getVersion());
        try {
            builder.withDetail("Version", version.getGitInformation().getBuildVersion());
            builder.withDetail("Branch", version.getGitInformation().getBranch());
            builder.withDetail("Commit ID", version.getGitInformation().getAbbreviatedCommitId());
            builder.withDetail("Dirty Directory", version.getGitInformation().getDirty());
            String drift = "A:" + version.getGitInformation().getLocalDirectoryAhead() + " B:" + version.getGitInformation().getLocalDirectoryBehind();
            builder.withDetail("Directory Drift", drift);
            builder.withDetail("Commit Time", version.getGitInformation().getLastCommitTime());
            builder.withDetail("Build Time", version.getGitInformation().getBuildTime());
            builder.withDetail("Build Host", version.getGitInformation().getBuildHost());
        }catch( Exception exception) {
            log.warn("Unable to get GIT information");
        }
    }
}
