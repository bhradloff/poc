package com.ayoholdings.ussd.system.version;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.util.StringUtils;

import java.util.Properties;

@Getter
@Setter
@ToString
public class GitInformation {
    private String branch;
    private String tags;
    private String remoteUrl;
    private String dirty;
    private String abbreviatedCommitId;
    private String fullCommitId;
    private String buildHost;
    private String buildVersion;
    private String buildTime;
    private String buildUser;
    private String build;

    private String localDirectoryAhead;
    private String localDirectoryBehind;
    private String lastCommitUser;
    private String lastCommitComment;
    private String lastCommitTime;

    public GitInformation(Properties properties) {
        this.branch = String.valueOf(properties.get("git.branch"));
        this.tags = String.valueOf(properties.get("git.tags"));
        this.dirty = String.valueOf(properties.get("git.dirty"));
        this.remoteUrl = String.valueOf(properties.get("git.remote.origin.url"));

        this.abbreviatedCommitId = String.valueOf(properties.get("git.id.abbrev"));
        this.fullCommitId = String.valueOf(properties.get("git.id.full"));
        if(!StringUtils.hasLength(this.abbreviatedCommitId) || "null".equalsIgnoreCase(this.abbreviatedCommitId)){
            this.abbreviatedCommitId = String.valueOf(properties.get("git.commit.id"));
            this.fullCommitId = String.valueOf(properties.get("git.commit.id"));
        }
        this.localDirectoryAhead = String.valueOf(properties.getProperty("git.local.branch.ahead"));
        this.localDirectoryBehind = String.valueOf(properties.getProperty("git.local.branch.behind"));
        this.buildHost = String.valueOf(properties.get("git.build.host"));
        this.buildVersion = String.valueOf(properties.get("git.build.version"));
        this.buildTime = String.valueOf(properties.get("git.build.time"));
        this.buildUser = String.valueOf(properties.get("git.build.user.name"));
        this.lastCommitTime = String.valueOf(properties.get("git.commit.committer.time"));
        this.lastCommitUser = properties.get("git.commit.user.name") + " : " + properties.get("git.commit.user.email");
        this.lastCommitComment = String.valueOf(properties.get("git.commit.message.full"));
    }
}
