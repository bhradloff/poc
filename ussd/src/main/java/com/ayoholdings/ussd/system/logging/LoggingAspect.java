package com.ayoholdings.ussd.system.logging;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.UUID;

@Slf4j
@Aspect
@Component
public class LoggingAspect {
    private final static String controllerPointCut = "execution(public * com.ayoholdings.ussd..*Controller.*(..))";

    @Before(controllerPointCut)
    public void preAction(){
    }

    private Object logAction(ProceedingJoinPoint joinPoint, String logDescription){
        Object returnData = null;
        UUID uuid = UUID.randomUUID();
        String callID = joinPoint.getSignature().getName() + ":" + uuid;
        Object[] args = joinPoint.getArgs();
        LocalDateTime startTime = LocalDateTime.now();
        log.info("message=Start of {} call[{}]&method={}&timestamp={}&args={}",
                logDescription,
                callID,
                callID,
                startTime.format(DateTimeFormatter.ISO_DATE_TIME),
                args);
        try {
            returnData = joinPoint.proceed();
        } catch (Throwable throwable) {
            log.error("message=Fatal error trying to execute the joinPoint: {} for method [{}]&method={}",
                    throwable.getMessage(),
                    callID,
                    callID,
                    throwable);
        }
        LocalDateTime endTime = LocalDateTime.now();
        long diff = ChronoUnit.MILLIS.between(startTime, endTime);
        log.info("message=End of {} call[{}]&method={}&timestamp={}&executionTime={}ms&returnData={}",
                logDescription,
                callID,
                callID,
                endTime.format(DateTimeFormatter.ISO_DATE_TIME),
                diff,
                returnData);
        return returnData;
    }

    @Around(controllerPointCut)
    public Object logAroundRestCalls(ProceedingJoinPoint joinPoint){
        return logAction(joinPoint, "REST");
    }

    @After(controllerPointCut)
    public void postAction(){
    }

    @Around("@annotation(com.ayoholdings.ussd.system.logging.Measure)")
    public Object recordMeasurement(ProceedingJoinPoint joinPoint){
        return logAction(joinPoint, "measured");
    }
}
