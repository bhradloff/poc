package com.ayoholdings.ussd.system.version;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Optional;
import java.util.Properties;

@Slf4j
@Service
public class VersionService {
    @Getter
    private String version;

    private GitInformation gitInfo;

    public VersionService(ApplicationContext context) {
        try {
            getGitInformation();
            version = gitInfo.getBuildVersion();
        } catch (Exception e) {
            log.warn("Git Information not available");
            if(version == null) {
                version = context.getBeansWithAnnotation(SpringBootApplication.class).entrySet().stream()
                        .findFirst()
                        .flatMap(es -> {
                            final String implementationVersion = es.getValue().getClass().getPackage().getImplementationVersion();
                            return Optional.ofNullable(implementationVersion);
                        }).orElse("development");
            }
        }
        log.info("Running Version [{}]", version);
    }

    public GitInformation getGitInformation() throws IOException {
        if (gitInfo == null) {
            Properties properties = new Properties();
            properties.load(getClass().getClassLoader().getResourceAsStream("git.properties"));
            gitInfo = new GitInformation(properties);
        }
        return gitInfo;
    }
}




