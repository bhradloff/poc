package com.ayoholdings.ussd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class UssdMenuGeneratorApplication {

	public static void main(String[] args) {
		SpringApplication.run(UssdMenuGeneratorApplication.class, args);
	}

}
