package com.ayoholdings.ussd.messages.ui;

import com.ayoholdings.ussd.messages.MessageResourceEntity;
import com.ayoholdings.ussd.messages.MessageResourceManagerService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;


/**
 * This controller is used for managing the user journeys
 */
@Slf4j
@Controller
@RequestMapping("/message")
@RequiredArgsConstructor
public class MessageResourceUIController {
    public final static String PAGE_MESSAGE = "message";

    private final String MESSAGE = "message";
    private final String JOURNEY_STEP_LIST = "messageList";

    private final @NonNull MessageResourceManagerService manager;

    @GetMapping()
    public String getMessages(@ModelAttribute MessageResourceEntity messageObj, Model model) {

        List<MessageResourceEntity> allMessages = manager.getAllMessages();

        model.addAttribute(JOURNEY_STEP_LIST, allMessages);
        model.addAttribute(MESSAGE, MessageResourceEntity.builder().build());

        return PAGE_MESSAGE;
    }

    @PostMapping()
    public String saveMessage(@ModelAttribute MessageResourceEntity messageObj, Model model) {

        manager.addMessageResource(messageObj.getMessageKey(), messageObj.getLocale(), messageObj.getSource(), messageObj.getValue());

        List<MessageResourceEntity> allMessages = manager.getAllMessages();

        model.addAttribute(JOURNEY_STEP_LIST, allMessages);
        model.addAttribute(MESSAGE, MessageResourceEntity.builder().build());
        return PAGE_MESSAGE;
    }

    @PostMapping("/delete")
    public String deleteMessage(@ModelAttribute MessageResourceEntity message, Model model){
        manager.deleteMessageResource(message.getMessageKey(), message.getSource(), message.getLocale());


        model.addAttribute(JOURNEY_STEP_LIST, manager.getAllMessages());
        model.addAttribute(MESSAGE, MessageResourceEntity.builder().build());
        return PAGE_MESSAGE;
    }
}
