package com.ayoholdings.ussd.messages;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.support.AbstractMessageSource;
import org.springframework.stereotype.Component;

import java.text.MessageFormat;
import java.util.Locale;

@Slf4j
@RequiredArgsConstructor
@Component
public class JourneyMessageSource extends AbstractMessageSource {
    private @NonNull MessageResourceRepository repo;
    private @NonNull MessageResourceManagerService manager;
    private static final String DEFAULT_LOCALE = "en";

    @Override
    protected MessageFormat resolveCode(String code, Locale locale) {
        String journeyMessage = manager.getJourneyMessage(code, locale.getLanguage());
        return new MessageFormat(journeyMessage, locale);
    }
}
