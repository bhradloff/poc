package com.ayoholdings.ussd.messages;

import com.ayoholdings.ussd.system.logging.Measure;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Slf4j
@Service
@RequiredArgsConstructor
public class MessageResourceManagerService {
    static private final String CACHE_MESSAGES = "messages";
    static private final String CACHE_MESSAGE_LOCALE = "messages-locale";
    static private final String CACHE_MESSAGES_LOCALE_SOURCE = "messages-locale-source";

    @NonNull private MessageResourceRepository repo;
    @NonNull private CacheManager cacheManager;

    @Measure
    public void addMessageResource(String key, String locale, String source, String value){
        MessageResourceEntity message = MessageResourceEntity.builder()
                .messageKey(key)
                .locale(locale)
                .value(value)
                .source(source)
                .build();
        repo.save(message);
        evictCaches();
    }

    public void deleteMessageResource(String key, String source, String locale) {
        List<MessageResourceEntity> messages = repo.findAllByMessageKeyAndSourceAndLocale(key, source, locale);
        log.info("Attempting to delete message {} {} {}", key, source, locale);
        log.info("found messages to delete = {}", messages.size());
        repo.deleteAll(messages);
        evictCaches();
    }


    @Measure
    @Cacheable(CACHE_MESSAGES)
    public List<MessageResourceEntity> getAllMessages(){
        return StreamSupport.stream(repo.findAll().spliterator(), false)
                .sorted((messageResourceEntity, t1) -> messageResourceEntity.getMessageKey().compareToIgnoreCase(t1.getMessageKey()))
                .collect(Collectors.toList());
    }


    @Measure
    @Cacheable(value=CACHE_MESSAGES_LOCALE_SOURCE, key="{#source, #locale}")
    public List<MessageResourceEntity> getAllMessagesForSourceAndLocale(String source, String locale){
        return StreamSupport.stream(repo.findAllBySourceAndLocale(source, locale).spliterator(), false).collect(Collectors.toList());
    }

    @Measure
    @Cacheable(value=CACHE_MESSAGE_LOCALE, key="{#key, #locale}")
    public String getJourneyMessage(String key, String locale){
        List<MessageResourceEntity> message = getAllMessagesForSourceAndLocale("message", locale);
        Optional<String> s = message.stream().filter(messageResourceEntity -> messageResourceEntity.getMessageKey().equals(key))
                .filter(messageResourceEntity -> messageResourceEntity.getLocale().equals(locale))
                .findFirst()
                .map(messageResourceEntity -> messageResourceEntity.getValue());
        return s.orElse("");
    }


//    @Caching(evict = {
//            @CacheEvict(value = CACHE_MESSAGES_LOCALE_SOURCE, allEntries=true),
//            @CacheEvict(value = CACHE_MESSAGES, allEntries=true)
//    })
    public void evictCaches(){
        log.info("Evicting the caches");
        cacheManager.getCache(CACHE_MESSAGES).clear();
        cacheManager.getCache(CACHE_MESSAGES_LOCALE_SOURCE).clear();
        cacheManager.getCache(CACHE_MESSAGE_LOCALE).clear();
    }


    /**
     * @TODO: This is for testing only, must be removed later
     * @param event
     */
    @EventListener
    public void init(ContextRefreshedEvent event){
        log.info("Initialising the messages");
        addMessageResource("KEY001", "en", "message", "Welcome String");
        addMessageResource("KEY002", "en", "message","2nd Screen");
        addMessageResource("KEY003", "en", "message","3rd Screen");
        addMessageResource("KEY004", "en", "message","4th Screen");
    }
}
