package com.ayoholdings.ussd.messages;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="messages")
@IdClass(MessageResourceEntity.MessageResourceKey.class)
public class MessageResourceEntity {
    @Id
    @Column(name="message_key")
    private String messageKey;
    @Id
    @Column(name="locale")
    private String locale;
    @Id
    private String source;

    private String value;

    @AllArgsConstructor
    @NoArgsConstructor
    public static class MessageResourceKey implements Serializable {
        protected String messageKey;
        protected String locale;
        protected String source;
    }
}
