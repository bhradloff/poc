package com.ayoholdings.ussd.messages;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface MessageResourceRepository extends CrudRepository<MessageResourceEntity, MessageResourceEntity.MessageResourceKey> {

    List<MessageResourceEntity> findAllBySourceAndLocale(String source, String locale);


    Optional<MessageResourceEntity> findByMessageKeyAndSourceAndLocale(String key, String source, String locale);

    @Query("select m from MessageResourceEntity m where m.messageKey = :key AND m.source = :source and m.locale = :locale")
    List<MessageResourceEntity> findAllByMessageKeyAndSourceAndLocale(@Param("key")String messageKey, @Param("source")String source, @Param("locale")String locale);
}
