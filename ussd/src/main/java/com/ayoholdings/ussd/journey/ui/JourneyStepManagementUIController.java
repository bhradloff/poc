package com.ayoholdings.ussd.journey.ui;

import com.ayoholdings.ussd.application.ApplicationManagerService;
import com.ayoholdings.ussd.journey.JourneyManagerService;
import com.ayoholdings.ussd.journey.JourneyStepEntity;
import com.ayoholdings.ussd.journey.ui.data.JourneyStep;
import com.ayoholdings.ussd.messages.MessageResourceManagerService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * This controller is used for managing the user journeys
 */
@Slf4j
@Controller
@RequestMapping(JourneyStepManagementUIController.PAGE_JOURNEY_STEP)
@RequiredArgsConstructor
public class JourneyStepManagementUIController {
    public static final String PAGE_JOURNEY_STEP = "/journey-step";

    private final String JOURNEY_STEP = "journeyStep";
    private final String JOURNEY_STEP_LIST = "journeyStepList";
    private final String JOURNEY_STEP_LIST_CURRENT = "journeyStepCurrentList";
    private final String MESSAGE_LIST = "messageList";
    private final String APPLICATION_LIST = "applicationList";

    private final @NonNull JourneyManagerService manager;
    private final @NonNull MessageResourceManagerService messages;
    private final @NonNull ApplicationManagerService applicationManager;

    @GetMapping()
    public String getMain(@ModelAttribute JourneyStep journeyStep, Model model) {
        model.addAttribute(JOURNEY_STEP_LIST, manager.getAllJourneySteps());
        model.addAttribute(JOURNEY_STEP_LIST_CURRENT, manager.findLastestSteps());
        model.addAttribute(MESSAGE_LIST, messages.getAllMessagesForSourceAndLocale("message", "en"));
        model.addAttribute(APPLICATION_LIST, applicationManager.getApplications());
        return PAGE_JOURNEY_STEP;
    }

    @PostMapping()
    public String createJourney(@ModelAttribute JourneyStep journeyStep, Model model) {
//    public String createJourney(Model model) {
        log.info("Using the journeyStep [{}] with a model of [{}]", journeyStep, model);
        if(journeyStep != null && journeyStep.getLabel() != null) {
            log.info("saving the journey step");
            JourneyStepEntity journeyStepEntity = manager.saveJourneyStep(journeyStep.getId(), journeyStep.getLabel(), journeyStep.getActivationCode(),
                    journeyStep.getServiceCode(), journeyStep.getMessageId(), journeyStep.getApplicationId(), journeyStep.getFirstStep());
            JourneyStep step = JourneyStep.fromEntity(journeyStepEntity);
            model.addAttribute(JOURNEY_STEP, step);
        }else {
            log.info("no journey step to save");
            model.addAttribute(JOURNEY_STEP, JourneyStep.builder().message("Nothing yet").build());
        }

        model.addAttribute(JOURNEY_STEP_LIST, manager.getAllJourneySteps());
        model.addAttribute(JOURNEY_STEP_LIST_CURRENT, manager.findLastestSteps());
        model.addAttribute(MESSAGE_LIST, messages.getAllMessagesForSourceAndLocale("message", "en"));
        model.addAttribute(APPLICATION_LIST, applicationManager.getApplications());
        return PAGE_JOURNEY_STEP;
    }
}
