package com.ayoholdings.ussd.journey;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Builder
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
//@IdClass(JourneyStepEntity.JourneyStepKey.class)
@Table(name = "journey_step")
public class JourneyStepEntity {

    @EmbeddedId
    private JourneyStepKey journeyStepKey;
//    @Id
//    @Column(name = "id")
//    private String id;
//    @Id
//    @Column(name = "version")
//    private Integer version;
    @Column(name = "label")
    private String label;
    @Column(name = "message_resource_id")
    private String messageResourceId;
    @Column(name = "application_service_id")
    private Long applicationServiceId;
    @Column(name = "activation_code")
    private String activationCode;

    @Column(name = "service_code")
    private String serviceCode;
    private Boolean firstStep;


    public Boolean equals(final JourneyStepEntity otherStep) {
        if(otherStep != null) {
            if(this.firstStep == null){
                this.setFirstStep(false);
            }
            if(this.activationCode.equals(otherStep.getActivationCode()) &&
                    this.applicationServiceId.equals(otherStep.getApplicationServiceId()) &&
                    this.label.equals(otherStep.getLabel()) &&
                    this.messageResourceId.equals(otherStep.getMessageResourceId()) &&
                    this.firstStep.equals(otherStep.getFirstStep()) &&
                    this.serviceCode.equals(otherStep.getServiceCode())
            ){
                return true;
            }
        }
        return false;
    }


}

