package com.ayoholdings.ussd.journey;

import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserJourneyRepository extends CrudRepository<UserJourneyEntity, Long> {

    public Optional<UserJourneyEntity> findFirstByMsisdnOrderByCreatedDesc(String msisdn);


}
