package com.ayoholdings.ussd.journey.api;

import com.ayoholdings.ussd.journey.JourneyManagerService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


/**
 * This controller is used for interacting with an external system to process ussd messagesJourneyRestController
 */
@RestController
@RequestMapping("/api/v1/ussd")
@RequiredArgsConstructor
public class JourneyRestController {

    private final @NonNull JourneyManagerService manager;

    @PostMapping
    public ResponseEntity<OutboundResponse> handleRequest(@RequestParam(required = false) String state) {
//        manager.getNextStep("dialString", "data");
        return ResponseEntity.ok(OutboundResponse.builder().build());
    }
}
