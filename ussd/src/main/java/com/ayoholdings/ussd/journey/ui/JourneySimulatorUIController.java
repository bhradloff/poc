package com.ayoholdings.ussd.journey.ui;

import com.ayoholdings.ussd.application.ApplicationManagerService;
import com.ayoholdings.ussd.journey.JourneyManagerService;
import com.ayoholdings.ussd.journey.UserJourneyManagerService;
import com.ayoholdings.ussd.journey.api.InboundRequest;
import com.ayoholdings.ussd.journey.ui.data.JourneyStep;
import com.ayoholdings.ussd.messages.MessageResourceManagerService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * This controller is used for managing the user journeys
 */
@Slf4j
@Controller
@RequestMapping(JourneySimulatorUIController.PAGE_JOURNEY_SIMULATOR)
@RequiredArgsConstructor
public class JourneySimulatorUIController {
    public static final String PAGE_JOURNEY_SIMULATOR = "/simulator";

    private final String JOURNEY_REQUEST = "request";
    private final String JOURNEY_REQUEST_RESPONSE = "journeyResponse";

    private final @NonNull JourneyManagerService manager;
    private final @NonNull MessageResourceManagerService messages;
    private final @NonNull UserJourneyManagerService userJourneyManagerService;
    private final @NonNull ApplicationManagerService applicationManager;

    @GetMapping()
    public String get(@ModelAttribute JourneyStep journeyStep, Model model) {
        model.addAttribute(JOURNEY_REQUEST, InboundRequest.builder().build());
        model.addAttribute(JOURNEY_REQUEST_RESPONSE, "");
        return PAGE_JOURNEY_SIMULATOR;
    }

    @PostMapping()
    public String post(@ModelAttribute InboundRequest request, Model model) {
//    public String createJourney(Model model) {
        log.info("Using the journeyStep [{}] with a model of [{}]", request, model);
        String nextMenu = userJourneyManagerService.getNextMenu(request);
        log.debug("Returning the menu:----------\n{}\n-----------", nextMenu);
        model.addAttribute(JOURNEY_REQUEST, InboundRequest.builder().build());
        model.addAttribute(JOURNEY_REQUEST_RESPONSE, nextMenu);

        return PAGE_JOURNEY_SIMULATOR;
    }
}
