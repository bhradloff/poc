package com.ayoholdings.ussd.journey.api;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class OutboundResponseData {
    private String inboundResponse;
    private Boolean userInputRequired;
}
