package com.ayoholdings.ussd.journey;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface JourneyStepRepository extends CrudRepository<JourneyStepEntity, String> {
    List<JourneyStepEntity> findByLabelOrderByJourneyStepKeyVersionDesc(String label);

    JourneyStepEntity findFirstByLabelOrderByJourneyStepKeyVersionDesc(String label);
    JourneyStepEntity findFirstByJourneyStepKeyIdOrderByJourneyStepKeyVersionDesc(String id);

//    String subQuery = "SELECT subStep.id FROM JourneyStepEntity subStep WHERE subStep.label = step.label AND subStep.version = (SELECT MAX(subSubStep.version) FROM JourneyStepEntity subSubStep where subSubStep.label = step.label)";
//    String mainQuery = "SELECT step FROM JourneyStepEntity step WHERE step.id IN (" + subQuery + ")";
    String maxVersion = "SELECT step FROM JourneyStepEntity step WHERE (step.journeyStepKey.id, step.journeyStepKey.version) IN (SELECT step2.journeyStepKey.id, MAX(step2.journeyStepKey.version) FROM JourneyStepEntity step2 GROUP BY step2.journeyStepKey.id) ORDER BY step.label";
    @Query(maxVersion)
    List<JourneyStepEntity> findMaxVersions();

    @Modifying
    @Query("update JourneyStepEntity js set js.firstStep = :state where  js.journeyStepKey.id = :id")
    void setFirstStepById(@Param("id") String id, @Param("state") Boolean state);

    @Modifying
    @Query("update JourneyStepEntity js set js.firstStep = :state where  js.journeyStepKey.id <> :id")
    void setFirstStepByNotId(@Param("id") String id, @Param("state") Boolean state);

    Optional<JourneyStepEntity> findFirstByFirstStepIsTrue();

}
