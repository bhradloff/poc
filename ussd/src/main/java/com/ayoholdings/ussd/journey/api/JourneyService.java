package com.ayoholdings.ussd.journey.api;


import com.ayoholdings.ussd.journey.JourneyRepository;
import com.ayoholdings.ussd.journey.JourneyStepEntity;
import com.ayoholdings.ussd.journey.JourneyStepRepository;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class JourneyService {

    private final @NonNull JourneyStepRepository journeyStepRepository;
    private final @NonNull JourneyRepository journeyRepository;

    public String getNextStep(String dialString, String data) {

        JourneyStepEntity step = JourneyStepEntity.builder()
                .activationCode("001")
                .build();
        JourneyStepEntity savedStep = journeyStepRepository.save(step);

        return savedStep.toString();
    }
}
