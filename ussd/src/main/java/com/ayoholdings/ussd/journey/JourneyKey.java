package com.ayoholdings.ussd.journey;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class JourneyKey implements Serializable {
    protected JourneyStepEntity currentJourneyStep;
    protected JourneyStepEntity nextJourneyStep;
}
