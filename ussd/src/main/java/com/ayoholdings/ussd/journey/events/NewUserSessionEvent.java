package com.ayoholdings.ussd.journey.events;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@ToString
@Builder
@Getter
public class NewUserSessionEvent  {
    private String msisdn;
}
