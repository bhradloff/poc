package com.ayoholdings.ussd.journey;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface JourneyRepository extends CrudRepository<JourneyEntity, String> {

    List<JourneyEntity> findAllByCurrentJourneyStepJourneyStepKeyId(String journeyStepId);
}
