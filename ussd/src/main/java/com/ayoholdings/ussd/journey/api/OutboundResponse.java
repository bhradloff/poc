package com.ayoholdings.ussd.journey.api;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class OutboundResponse {
    private String statusCode;
    private String statusMessage;
    private String transactionId;
    private OutboundResponseData data;
//    private Links link;
}

