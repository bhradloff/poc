package com.ayoholdings.ussd.journey;

import com.ayoholdings.ussd.system.logging.Measure;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Slf4j
@Service
@RequiredArgsConstructor
public class JourneyManagerService {

    private static final String CACHE_JOURNEY_STEP = "journey-step-all";
    private static final String CACHE_JOURNEY_STEP_SINGLE_LAST = "journey-step-latest-last";
    private static final String CACHE_JOURNEY_STEP_LATEST = "journey-step-latest";
    private static final String CACHE_JOURNEY_LATEST = "journey-latest";

    private final @NonNull JourneyStepRepository journeyStepRepository;
    private final @NonNull JourneyRepository journeyRepository;
    private final @NonNull CacheManager cacheManager;

    /**
     * These are methods to maintain a Journey
     */
    @Measure
    public void saveJourney(String currentStep, String targetStep, Boolean directAccess, String response, String label) {
        JourneyStepEntity currentStepEntity = journeyStepRepository.findFirstByJourneyStepKeyIdOrderByJourneyStepKeyVersionDesc(currentStep);
        log.info("Found the source step: {}", currentStepEntity);
        JourneyStepEntity targetStepEntity = journeyStepRepository.findFirstByJourneyStepKeyIdOrderByJourneyStepKeyVersionDesc(targetStep);
        log.info("Found the target step: {}", targetStepEntity);
        JourneyEntity newJourney = JourneyEntity.builder().active(false)
                .id(UUID.randomUUID().toString())
                .currentJourneyStep(currentStepEntity)
                .nextJourneyStep(targetStepEntity)
                .validResponse(response)
                .directAccess(directAccess)
                .label(label)
//                .addParameter(null, null)
                .build();
        journeyRepository.save(newJourney);
        clearCaches(); // things have changed clear the caches
    }

    @Measure
    @Cacheable(CACHE_JOURNEY_LATEST)
    public List<JourneyEntity> getJourneys(){
        List<JourneyEntity> allJourneys = StreamSupport.stream(journeyRepository.findAll().spliterator(), false).collect(Collectors.toList());
        log.debug("getJourneys is returning {} rows", allJourneys.size());
        return allJourneys;
    }

    public void updateJourney() {

    }

    @Measure
    public void deleteJourney(String id) {
        journeyRepository.deleteById(id);
        clearCaches();
    }

    @Measure
    @Transactional(propagation = Propagation.REQUIRED)
    public JourneyStepEntity saveJourneyStep(String id, String label, String activationCode, String serviceCode, String messageId, Long applicationId, Boolean firstStep) {
        boolean saveStep = false;
        JourneyStepEntity stepParameters = JourneyStepEntity.builder().activationCode(activationCode).label(label).serviceCode(serviceCode).messageResourceId(messageId).applicationServiceId(applicationId).build();

        List<JourneyStepEntity> existingSteps = journeyStepRepository.findByLabelOrderByJourneyStepKeyVersionDesc(label);
        JourneyStepEntity.JourneyStepEntityBuilder journeyStepBuilder = JourneyStepEntity.builder();
        if (existingSteps.size() > 0) {
            JourneyStepEntity journeyStepEntity = existingSteps.get(0);
            if (!stepParameters.equals(journeyStepEntity)) {
                saveStep = true;
            }
            JourneyStepKey journeyStepKey = journeyStepEntity.getJourneyStepKey();
            journeyStepKey.setVersion(journeyStepKey.getVersion() + 1);
            journeyStepBuilder.journeyStepKey(journeyStepKey);
            journeyStepBuilder.label(journeyStepEntity.getLabel());
        } else {
            saveStep = true;
            JourneyStepKey journeyStepKey = new JourneyStepKey();
            journeyStepKey.setVersion(1);
            journeyStepKey.setId(UUID.randomUUID().toString());
            journeyStepBuilder.journeyStepKey(journeyStepKey);
        }
        JourneyStepEntity journeyStep = journeyStepBuilder
                .activationCode(activationCode)
                .applicationServiceId(applicationId)
                .serviceCode(serviceCode)
                .label(label)
                .messageResourceId(messageId)
                .firstStep(firstStep)
                .build();

        //we only allow one first step
        if(firstStep) {
            journeyStepRepository.setFirstStepById(journeyStep.getJourneyStepKey().getId(), firstStep);
            journeyStepRepository.setFirstStepByNotId(journeyStep.getJourneyStepKey().getId(), !firstStep);
        }

        JourneyStepEntity savedJourney = stepParameters;
        if (saveStep) {
            savedJourney = journeyStepRepository.save(journeyStep);
            clearCaches();  //clear the caches, things have changed
        }
        return savedJourney;
    }

    public void clearCaches() {
        cacheManager.getCache(CACHE_JOURNEY_STEP).clear();
        cacheManager.getCache(CACHE_JOURNEY_STEP_LATEST).clear();
        cacheManager.getCache(CACHE_JOURNEY_STEP_SINGLE_LAST).clear();
        cacheManager.getCache(CACHE_JOURNEY_LATEST).clear();
    }

    @Measure
    @Cacheable(CACHE_JOURNEY_STEP)
    public List<JourneyStepEntity> getAllJourneySteps() {
        Iterable<JourneyStepEntity> allSteps = journeyStepRepository.findAll();
        List<JourneyStepEntity> listOfSteps = StreamSupport.stream(allSteps.spliterator(), false).collect(Collectors.toList());
        return listOfSteps;
    }

    @Measure
    @Cacheable(CACHE_JOURNEY_STEP_SINGLE_LAST)
    public JourneyStepEntity findLastestStepforLabel(String label) {
        return journeyStepRepository.findFirstByLabelOrderByJourneyStepKeyVersionDesc(label);
    }

    @Measure
    @Cacheable(CACHE_JOURNEY_STEP_LATEST)
    public List<JourneyStepEntity> findLastestSteps() {
        List<JourneyStepEntity> maxVersions = journeyStepRepository.findMaxVersions();
        log.debug("Returning {} rows", maxVersions.size());
        return maxVersions;
    }

    public void updateJourneyStep() {

    }

    public void deleteJourneyStep() {

    }
}
