package com.ayoholdings.ussd.journey.ui;

import com.ayoholdings.ussd.journey.JourneyEntity;
import com.ayoholdings.ussd.journey.JourneyManagerService;
import com.ayoholdings.ussd.journey.ui.data.JourneyInput;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Slf4j
@Controller
@RequiredArgsConstructor
@RequestMapping("/" + JourneyManagementUIController.PAGE_JOURNEY)
public class JourneyManagementUIController {
    public static final String PAGE_JOURNEY = "journey";
    public static final String MODEL_STEPS = "journey_steps";
    public static final String MODEL_JOURNEY = "journey";
    public static final String MODEL_JOURNEYS = "journeys";

    @NonNull JourneyManagerService manager;


    @GetMapping
    public String getJourneys(Model model){
        model.addAttribute(MODEL_STEPS, manager.findLastestSteps());
        model.addAttribute(MODEL_JOURNEYS, manager.getJourneys());
        model.addAttribute(MODEL_JOURNEY, JourneyInput.builder().build());
        model.addAttribute("journey_diagram", generateJourneyDiagram());
        return PAGE_JOURNEY;
    }

    @PostMapping
    public String saveJourneys(@ModelAttribute("journey") JourneyInput journey, Model model){
        log.info("Received Journey {}", journey);
        if(StringUtils.hasText(journey.getResponse())) {
            try {
                manager.saveJourney(journey.getSource(), journey.getTarget(), false, journey.getResponse(), journey.getLabel());
            } catch (Exception e) {
                model.addAttribute("errorMessage", "Journey not saved. "  + e.getMessage());
                log.error("Journey not saved: {}", e.getMessage(), e);
            }
        }else{
            model.addAttribute("errorMessage", "Journey not saved. No valid response given");
        }

        model.addAttribute(MODEL_STEPS, manager.findLastestSteps());
        model.addAttribute(MODEL_JOURNEYS, manager.getJourneys());
        model.addAttribute(MODEL_JOURNEY, JourneyInput.builder().build());
        model.addAttribute("journey_diagram", generateJourneyDiagram());
        return PAGE_JOURNEY;
    }

    @PostMapping("/delete")
    public String deleteJourney(@ModelAttribute("journey") JourneyInput journey, Model model){
        log.info("Received Journey {}", journey);
        if(StringUtils.hasText(journey.getId())) {
            try {
                manager.deleteJourney(journey.getId());
            } catch (Exception e) {
                model.addAttribute("errorMessage", "Journey not DELETED. " + e.getMessage());
                log.error("Journey not DELETED: {}", e.getMessage(), e);
            }
        }else{
            model.addAttribute("errorMessage", "Journey not DELETED. No valid identifier given");
        }

        model.addAttribute(MODEL_STEPS, manager.findLastestSteps());
        model.addAttribute(MODEL_JOURNEYS, manager.getJourneys());
        model.addAttribute(MODEL_JOURNEY, JourneyInput.builder().build());
        model.addAttribute("journey_diagram", generateJourneyDiagram());
        return PAGE_JOURNEY;
    }



    private String generateJourneyDiagram(){
        List<JourneyEntity> journeys = manager.getJourneys();
        if(journeys.size() > 0) {
            StringBuilder journeyBuilder = new StringBuilder("flowchart LR\n");
            Set<String> uniqueJourneys = new HashSet<>();
            journeys.stream().forEach(journeyEntity -> {
                uniqueJourneys.add(journeyEntity.getCurrentJourneyStep().getLabel());
                uniqueJourneys.add(journeyEntity.getNextJourneyStep().getLabel());
            });
            uniqueJourneys.stream().forEach(s -> journeyBuilder.append(s.replaceAll(" ", "_"))
                    .append("[")
                    .append(s)
                    .append("]\n"));
            journeys.stream().forEach(j -> {
                journeyBuilder.append(j.getCurrentJourneyStep().getLabel().replaceAll(" ", "_"))
                        .append(" -- ")
                        .append(j.getValidResponse())
                        .append(" --> ")
                        .append(j.getNextJourneyStep().getLabel().replaceAll(" ", "_"))
                        .append("\n");
            });
            log.debug("{}", journeyBuilder.toString());
            return journeyBuilder.toString();
        }
        return "";
    }
}
