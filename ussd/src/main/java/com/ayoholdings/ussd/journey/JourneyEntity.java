package com.ayoholdings.ussd.journey;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Arrays;
import java.util.HashMap;

@Builder
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "journey")
//@IdClass(JourneyKey.class)
public class JourneyEntity {
    private final static String PARAM_DISCIMINATOR = "###";

    @Id
    private String id;
    @ManyToOne
    @JoinColumns({
            @JoinColumn(name = "current_step_id", referencedColumnName = "id"),
            @JoinColumn(name = "current_step_version", referencedColumnName = "version")
    } )
    private JourneyStepEntity currentJourneyStep;
    @ManyToOne
    @JoinColumns({
            @JoinColumn(name = "next_step_id", referencedColumnName = "id"),
            @JoinColumn(name = "next_step_version", referencedColumnName = "version")
    } )
    private JourneyStepEntity nextJourneyStep;

    @Column(name="valid_response")
    private String validResponse;
    @Column(name="version")
    private Integer version;
    @Column(name="active")
    private Boolean active;
    @Column(name="direct_access")
    private Boolean directAccess;
    private String label;

    @Column(name="parameters")
        private String parameters;

    @Transient
    @Getter(AccessLevel.PRIVATE)
    @Setter(AccessLevel.PRIVATE)
    private HashMap<String, Object> params = null;

    public String getParameters(){
        convertToParameterString();
        return parameters;
    }

    public HashMap<String, Object> getParameterMap(){
        if(parameters != null && parameters.length() > 0 && params == null){
            params = new HashMap<>();
            String[] parms = parameters.split(PARAM_DISCIMINATOR);
            Arrays.stream(parms).forEach(param -> {
                if(param!= null && param.length()>2) {
                    String[] valuePair = param.split("=");
                    if (valuePair.length == 2) {
                        params.put(valuePair[0], valuePair[1]);
                    }
                }
            });
        }
        return params;
    }

    public void addParameter(String key, Object value) {
        if(params==null){
            params = new HashMap<>();
        }
        params.put(key, value);
        convertToParameterString();
    }
    private void convertToParameterString() {
        final StringBuilder parameters = new StringBuilder();
        if(params == null){
            params = new HashMap<>();
        }
        params.forEach((s, o) -> {
            if(parameters.length() == 0) {
                parameters.append(s).append("=").append(o.toString());
            } else {
                parameters.append(PARAM_DISCIMINATOR).append(s).append("=").append(o.toString());
            }
        });
        this.parameters = parameters.toString();
    }


    public static class JourneyEntityBuilder {
        public JourneyEntityBuilder addParameter(String key, Object value){
            this.params.put(key, value);
            return this;
        }
    }


}

