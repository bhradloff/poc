package com.ayoholdings.ussd.journey;

import com.ayoholdings.ussd.application.ApplicationManagerService;
import com.ayoholdings.ussd.application.ApplicationService;
import com.ayoholdings.ussd.journey.api.InboundRequest;
import com.ayoholdings.ussd.journey.events.NewUserSessionEvent;
import com.ayoholdings.ussd.messages.MessageResourceManagerService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

@Slf4j
@RequiredArgsConstructor
@Service
public class UserJourneyManagerService {
    private final @NonNull JourneyStepRepository journeyStepRepository;
    private final @NonNull MessageResourceManagerService messageService;
    private final @NonNull MessageSource journeyMessageSource;
    private final @NonNull JourneyRepository journeyRepository;
    private final @NonNull UserJourneyRepository userJourneyRepository;
    private final @NonNull CacheManager cacheManager;
    private final @NonNull JourneyManagerService journeyManagerService;
    private final @NonNull ApplicationManagerService applicationService;
    private final @NonNull ApplicationEventPublisher publisher;


    @Value("${session.timeout:300}")
    private Integer timeout;

    public String getNextMenu(InboundRequest request) {
        String menuResponse = "";
        final String userInput = request.getUssdString();
        String msisdn = request.getMSISDN();

        Optional<UserJourneyEntity> optionalUserJourney = userJourneyRepository.findFirstByMsisdnOrderByCreatedDesc(msisdn);
        if (optionalUserJourney.isPresent()) {
            UserJourneyEntity existingJourney = optionalUserJourney.get();
            //we have an existing session, is it expired?
            //check for time out of session
            if (existingJourney.getCreated() == null || existingJourney.getCreated().plusSeconds(timeout).isBefore(LocalDateTime.now())) {
                //timed out
                //New session and new user...
                menuResponse = generateNewUserSessionMenu(NewUserSessionEvent.builder().msisdn(msisdn).build());
            } else {
                JourneyStepKey journeyStepKey = existingJourney.getStep().getJourneyStepKey();

                List<JourneyEntity> journeys = journeyRepository.findAllByCurrentJourneyStepJourneyStepKeyId(journeyStepKey.getId());
                Optional<JourneyEntity> journeyOptional = journeys.stream().filter(journeyEntity -> journeyEntity.getValidResponse().toString().equals(userInput)).findFirst();
                if(journeyOptional.isPresent()){
                    existingJourney.setResponse(userInput);
                    existingJourney.setModified(LocalDateTime.now());
                    userJourneyRepository.save(existingJourney);
                    JourneyEntity currentJourney = journeyOptional.get();
                    JourneyStepEntity nextJourneyStep = currentJourney.getNextJourneyStep();
                    log.info("{} {}", currentJourney, nextJourneyStep);
                    menuResponse = generateExistingUserSessionMenu(msisdn, nextJourneyStep);
                }
                log.info("{}", journeys);
            }
        } else {
            //New session and new user...
            menuResponse = generateNewUserSessionMenu(NewUserSessionEvent.builder().msisdn(msisdn).build());

        }
        return menuResponse;
    }

    private String generateExistingUserSessionMenu(String msisdn, JourneyStepEntity nextStep){
        StringBuilder response = new StringBuilder();

        List<JourneyEntity> journeys = journeyRepository.findAllByCurrentJourneyStepJourneyStepKeyId(nextStep.getJourneyStepKey().getId());
        journeys.stream().findFirst().stream().forEach(journeyEntity -> {
            response
                    .append(getMessage(journeyEntity.getCurrentJourneyStep().getMessageResourceId(), "en", journeyEntity.getCurrentJourneyStep().getApplicationServiceId()))
                    .append("\n");
        });
        journeys.stream()
                .sorted((o1, o2) -> {
                    if (o1 == null || o1.getValidResponse() == null || o2 == null || o2.getValidResponse() == null) {
                        return -1;
                    }
                    Integer o1Value = Integer.parseInt(o1.getValidResponse());
                    Integer o2Value = Integer.parseInt(o2.getValidResponse());
                    int comparedResult = o1Value.compareTo(o2Value);
                    log.debug("Comparing {} to {} with the result {}", o1.getValidResponse(), o2.getValidResponse(), comparedResult);
                    return comparedResult;
                })
                .forEach(journeyEntity -> {
                    log.debug("journeyEntity [{}]", journeyEntity);
                    response.append(journeyEntity.getValidResponse())
                            .append(". ")
                            .append(journeyEntity.getLabel())
                            .append("\n");
                });

        userJourneyRepository.save(UserJourneyEntity.builder()
                .msisdn(msisdn)
                .step(nextStep)
                .created(LocalDateTime.now())
                .build());
        return response.toString();

    }
//
//    private UserJourneyEntity determinCurrentUserStep(String msisdn, String response) {
//        UserJourneyEntity nextJourney = null;
//        Optional<UserJourneyEntity> optionalUserJourney = userJourneyRepository.findFirstByMsisdnOrderByCreatedDesc(msisdn);
//        //this means there is an existing journey in-flight
//        if (optionalUserJourney.isPresent()) {
//            UserJourneyEntity existingJourney = optionalUserJourney.get();
//            //we have an existing session, is it expired?
//            //check for time out of session
//            if (existingJourney.getCreated() == null || existingJourney.getCreated().plusSeconds(timeout).isAfter(LocalDateTime.now())) {
//                //timed out
//                //New session and new user...
//                nextJourney = optionalUserJourney.get();
//                publisher.publishEvent(NewUserSessionEvent.builder().msisdn(msisdn).build());
//            } else {
//
//            }
//        } else {
//            nextJourney = optionalUserJourney.get();
//            publisher.publishEvent(NewUserSessionEvent.builder().msisdn(msisdn).build());
//        }
//
//        return nextJourney;
//    }

    private String getMessage(String key, String locale, Long serviceId) {
        String message = null;
        Optional<ApplicationService> optionalService = this.applicationService.getApplicationService(serviceId);
        if (optionalService.isPresent()) {
            ApplicationService applicationService = optionalService.get();
            Object[] messageVariables = applicationService.getMessageVariables();
            message = journeyMessageSource.getMessage(key, messageVariables, Locale.forLanguageTag(locale));
        }
        return message;
    }

    @EventListener
    public String generateNewUserSessionMenu(NewUserSessionEvent event) {
        StringBuilder response = new StringBuilder();
        Optional<JourneyStepEntity> optionalFirstStep = journeyStepRepository.findFirstByFirstStepIsTrue();

        if (optionalFirstStep.isPresent()) {
            JourneyStepEntity journeyStepEntity = optionalFirstStep.get();
            List<JourneyEntity> journeys = journeyRepository.findAllByCurrentJourneyStepJourneyStepKeyId(journeyStepEntity.getJourneyStepKey().getId());

            journeys.stream().findFirst().stream().forEach(journeyEntity -> {
                response
                        .append(getMessage(journeyEntity.getCurrentJourneyStep().getMessageResourceId(), "en", journeyEntity.getCurrentJourneyStep().getApplicationServiceId()))
                        .append("\n");
            });
            journeys.stream()
                    .sorted((o1, o2) -> {
                        if (o1 == null || o1.getValidResponse() == null || o2 == null || o2.getValidResponse() == null) {
                            return -1;
                        }
                        Integer o1Value = Integer.parseInt(o1.getValidResponse());
                        Integer o2Value = Integer.parseInt(o2.getValidResponse());
                        int comparedResult = o1Value.compareTo(o2Value);
                        log.debug("Comparing {} to {} with the result {}", o1.getValidResponse(), o2.getValidResponse(), comparedResult);
                        return comparedResult;
                    })
                    .forEach(journeyEntity -> {
                        log.debug("journeyEntity [{}]", journeyEntity);
                        response.append(journeyEntity.getValidResponse())
                                .append(". ")
                                .append(journeyEntity.getLabel())
                                .append("\n");
                    });

            userJourneyRepository.save(UserJourneyEntity.builder()
                            .msisdn(event.getMsisdn())
                            .step(journeyStepEntity)
                            .created(LocalDateTime.now())
                    .build());

        } else {
            //dreadful error in configuration for the user... where to start?
        }
        return response.toString();
    }


    public void processCurrentStep() {

    }

    public void preProcessCurrentStep() {

    }

    public void postProcessCurrentStep() {

    }
}
