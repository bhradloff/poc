package com.ayoholdings.ussd.journey;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;

@ToString
@Builder
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "user_journey")
public class UserJourneyEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    @ManyToOne
    @JoinColumns({
            @JoinColumn(name = "stepId", referencedColumnName = "id"),
            @JoinColumn(name = "stepVersion", referencedColumnName = "version")
    } )
    private JourneyStepEntity step;
    private String msisdn;
    private String response;
    private Integer sequence;
    private Boolean directAccess;
    private LocalDateTime created;
    private LocalDateTime modified;
}
