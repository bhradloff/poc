package com.ayoholdings.ussd.journey.ui.data;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class JourneyInput {
    private String id;
    private String source;
    private String response;
    private String target;
    private String label;
}
