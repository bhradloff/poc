package com.ayoholdings.ussd.journey.ui.data;

import com.ayoholdings.ussd.journey.JourneyStepEntity;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Builder
@Data
public class JourneyStep implements Serializable {
    private String id;
    private String label;
    private String activationCode;
    private String serviceCode;
    private String messageId;
    private String message;
    private Integer version;
    private Long applicationId;
    private Boolean firstStep;

    public static JourneyStep fromEntity(JourneyStepEntity entity){

        JourneyStepBuilder builder = JourneyStep.builder();
        if(entity.getJourneyStepKey() != null){
            builder.id(entity.getJourneyStepKey().getId())
                    .version(entity.getJourneyStepKey().getVersion());
        }

        return builder
                .label(entity.getLabel())
                .activationCode(entity.getActivationCode())
                .serviceCode(entity.getServiceCode())
                .messageId(entity.getMessageResourceId())
                .applicationId(entity.getApplicationServiceId())
                .firstStep(entity.getFirstStep())
                .build();

    }
}
