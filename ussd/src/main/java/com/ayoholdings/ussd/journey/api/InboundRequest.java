package com.ayoholdings.ussd.journey.api;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class InboundRequest {
        private String sessionId;
        private int messageType =-1;
        private String MSISDN;
        private String serviceCode;
        private String ussdString;
        private String cellId;
        private String language;
        private String imsi;
}
