package com.ayoholdings.ussd.application;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@ToString
@Builder
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "application")
public class ApplicationEntity {
    @Id
    Long id;
    String name;
    Integer version;
    String type;
    Boolean generatesJourney;
    String parametersAvailable;
}
