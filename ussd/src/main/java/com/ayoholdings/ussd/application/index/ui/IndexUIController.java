package com.ayoholdings.ussd.application.index.ui;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/ui")
public class IndexUIController {
    @GetMapping()
    public String getIndex( Model model) {
        return "index";
    }
}
