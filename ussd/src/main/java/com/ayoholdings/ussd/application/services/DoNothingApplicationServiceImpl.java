package com.ayoholdings.ussd.application.services;

import com.ayoholdings.ussd.application.ApplicationServiceRegistry;
import org.springframework.stereotype.Service;

import static com.ayoholdings.ussd.application.services.DoNothingApplicationServiceImpl.BEAN_NAME;

@Service(BEAN_NAME)
public class DoNothingApplicationServiceImpl extends DefaultApplicationService{

    public static final String BEAN_NAME = "doNothing";

    public DoNothingApplicationServiceImpl(){
        super(ApplicationServiceRegistry.DO_NOTHING, false);
    }

    @Override
    public Boolean process() {
        return null;
    }

    @Override
    public Boolean postProcess() {
        return null;
    }

    @Override
    public Boolean preProcess() {
        return null;
    }
}
