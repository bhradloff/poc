package com.ayoholdings.ussd.application;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RequiredArgsConstructor
@Slf4j
@Service
public class ApplicationManagerService {
    @NonNull ApplicationServiceRepository repo;
    @NonNull ApplicationContext context;

    @Cacheable("applications")
    public List<ApplicationEntity> getApplications(){
        List<ApplicationEntity> all = StreamSupport.stream(repo.findAll().spliterator(), false).collect(Collectors.toList());
        return all;
    }

    public Optional<ApplicationService> getApplicationService(String serviceName){
        ApplicationService service = context.getBean(serviceName, ApplicationService.class);
        if(service != null) {
            log.debug("Returning the bean associated with the service name [{}]", serviceName);
            return Optional.of(service);
        }
        log.error("Unable to find the service bean with the name [{}]", serviceName);
        return Optional.empty();
    }

    public Optional<ApplicationService> getApplicationService(Long serviceId){
        Optional<ApplicationEntity> optionalService = repo.findById(serviceId);
        if(optionalService.isPresent()) {
            return getApplicationService(optionalService.get().getName());
        }
        log.error("Unable to find the service with the id [{}]", serviceId);
        return Optional.empty();
    }
}
