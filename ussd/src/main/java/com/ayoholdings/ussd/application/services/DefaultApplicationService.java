package com.ayoholdings.ussd.application.services;

import com.ayoholdings.ussd.application.ApplicationEntity;
import com.ayoholdings.ussd.application.ApplicationService;
import com.ayoholdings.ussd.application.ApplicationServiceRegistry;
import com.ayoholdings.ussd.application.ApplicationServiceRepository;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ExitCodeGenerator;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;

import java.util.Optional;


@Slf4j
@RequiredArgsConstructor
public abstract class DefaultApplicationService implements ApplicationService, ExitCodeGenerator {

    //Only needed by the base abstract class
    @Autowired private ConfigurableApplicationContext context;
    @Autowired protected ApplicationServiceRepository repository;

    //Needs to be exposed to the implementing class
    @NonNull protected ApplicationServiceRegistry registry;
    @NonNull protected Boolean generatesJourney;


    @Override
    public ApplicationEntity registerApplication(){
        ApplicationEntity app = ApplicationEntity.builder()
                .id(Integer.toUnsignedLong(registry.ordinal()))
                .generatesJourney(false)
                .parametersAvailable("")
                .name(registry.getServiceName())
                .type("Application Service")
                .version(1)
                .build();
        ApplicationEntity savedApplication = repository.save(app);
        return savedApplication;
    }

    public Object[] getMessageVariables() {
        //We should be looking up the clients name etc here
        return new Object[0];
    }

    @EventListener
    public void register(ContextRefreshedEvent event) {
        if (registry == null) {
            log.error("Application service does not have a registry entry");
            terminateApplication();
        }
        int ordinal = registry.ordinal();
        log.debug("Application is attempting to register as {} with id {}", registry.getServiceName(), ordinal);
        Optional<ApplicationEntity> byId = repository.findById(Integer.toUnsignedLong(ordinal));
        if (byId.isEmpty()) {
            ApplicationEntity applicationEntity = this.registerApplication();
            if(applicationEntity == null){
                log.error("Application [name: {}] [id:{}] did not register", registry.getServiceName(), ordinal);
                terminateApplication();
            }
        } else {
            if(!byId.get().getName().equals(registry.getServiceName())) {
                log.error("Application [new:{}, old: {}] is attempting to register with the same information [id:{}] as a previous service",
                        registry.getServiceName(), byId.get().getName(), ordinal);
                terminateApplication();
            }
        }
        log.info("Application [{}] has been registered with id [{}]", registry.getServiceName(), ordinal);
    }
    private void terminateApplication(){
        context.close();
    }

    @Override
    public int getExitCode() {
        return 10111;
    }

}
