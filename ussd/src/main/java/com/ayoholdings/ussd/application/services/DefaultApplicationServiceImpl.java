package com.ayoholdings.ussd.application.services;

import com.ayoholdings.ussd.application.ApplicationServiceRegistry;
import org.springframework.stereotype.Service;

import static com.ayoholdings.ussd.application.services.DefaultApplicationServiceImpl.BEAN_NAME;

@Service(BEAN_NAME)
public class DefaultApplicationServiceImpl extends DefaultApplicationService{

    private static final Integer numberOfVariablesProvided = 0;
    public static final String BEAN_NAME = "defaultApplication";
    public DefaultApplicationServiceImpl(){
        super(ApplicationServiceRegistry.DEFAULT, false);
    }

    @Override
    public Object[] getMessageVariables() {
        Object[] messageVariables = super.getMessageVariables();
        Object[] revisedSet = new Object[messageVariables.length + numberOfVariablesProvided];
        for (int index = 0; index < messageVariables.length; index++) {
            revisedSet[index] = messageVariables[index];
        }
        return revisedSet;
    }

    @Override
    public Boolean process() {
        return null;
    }

    @Override
    public Boolean postProcess() {
        return null;
    }

    @Override
    public Boolean preProcess() {
        return null;
    }
}
