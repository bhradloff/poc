package com.ayoholdings.ussd.application;

import com.ayoholdings.ussd.application.services.DefaultApplicationServiceImpl;
import com.ayoholdings.ussd.application.services.DoNothingApplicationServiceImpl;
import lombok.Getter;

public enum ApplicationServiceRegistry {
    // This order should not change once it is set
    DEFAULT(DefaultApplicationServiceImpl.BEAN_NAME),
    DO_NOTHING(DoNothingApplicationServiceImpl.BEAN_NAME)
    ;

    @Getter
    private String serviceName;
    @Getter
    private Integer id;

    private ApplicationServiceRegistry(String name){
        this.id = this.ordinal();
        this.serviceName = name;
    }
}
