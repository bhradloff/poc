package com.ayoholdings.ussd.application.ui;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;



@Slf4j
@Controller
@RequestMapping(ApplicationUIController.PAGE_APPLICATIION)
public class ApplicationUIController {
    public static final String PAGE_APPLICATIION = "/application";
}
