package com.ayoholdings.ussd.application;

public interface ApplicationService {
    ApplicationEntity registerApplication();

    Object[] getMessageVariables();
    Boolean process();
    Boolean postProcess();
    Boolean preProcess();
}
