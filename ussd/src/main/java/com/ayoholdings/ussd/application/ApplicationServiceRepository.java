package com.ayoholdings.ussd.application;

import org.springframework.data.repository.CrudRepository;

public interface ApplicationServiceRepository extends CrudRepository<ApplicationEntity, Long> {
}
