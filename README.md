# Development Playground Bucket
This repository is for the purpose of saving projects that do not have a specific reason, other that testing concepts.

Please try to keep it neat and tidy

Current Sandboxes
* Spring Cloud Data Flow
```
Testing and proofing of SCDF concepts, currently aimed at getting batch working
```